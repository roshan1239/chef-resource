Chef file locations

Posted on January 4, 2017 by Laszlo Pinter
The Chef file and folder locations are different on Linux and Windows machines. This article explains the purpose of each file and the location.

Summary

Linux	Windows
Cookbook location	/var/chef/cache/cookbooks	 C:\chef\cache\cookbooks
Chef Client run log	/var/log/chef.log	First run only
C:\chef\chef-client.log
Subsequent Chef client runs
C:\chef\log\client.log
Error log	/var/chef/cache/chef-stacktrace.out	C:\chef\cache\chef-stacktrace.out
Ohai output	/var/chef/cache/failed-run-data.json	C:\chef\cache\failed-run-data.json
Recommended location for custom log files	/tmp/cheflog.log	C:\Logs\Chef\cheflog.log
Chef Client configuration	/etc/chef/client.rb	C:\chef\client.rb
When you test your cookbook in Test Kitchen

The .kitchen.yml file contains the username to execute the Chef cookbook. It is specified under platforms:, transport:, username:

Use that value in place of USER-NAME-FROM-KITCHEN-YML below.

Linux	Windows
Cookbook location	/tmp/kitchen/cookbooks
/tmp/kitchen/cache/cookbooks	 C:\Users\USER-NAME-FROM-KITCHEN-YML\AppData\Local\Temp\kitchen\cookbooks
Error log	/tmp/kitchen/cache/chef-stacktrace.out	C:\Users\USER-NAME-FROM-KITCHEN-YML\AppData\Local\Temp\kitchen\cache\chef-stacktrace.out
Ohai output	/tmp/kitchen/cache/failed-run-data.json	C:\Users\USER-NAME-FROM-KITCHEN-YML\AppData\Local\Temp\kitchen\cache\failed-run-data.json
Data bags	/tmp/kitchen/data_bags	C:\Users\USER-NAME-FROM-KITCHEN-YML\AppData\Local\Temp\kitchen\data_bags
Cookbook location

When the Chef recipes are executed, all cookbooks are stored on the node. You can examine the code to make sure your latest changes are reflected on the machine.

The log of the Chef client run

The output of the Chef cookbook execution is in the chef.log or chef-client.log file

On Windows

The log of the first Chef Client run and subsequent runs are stored in different log files. After the initial Chef Client run, the rest of the log entries are collected in the second file.

Stacktrace

Chef saves information on the hard drive when scripts are executed. If there is a failure, the stack trace of the last error is saved in the chef-stacktrace.out file.

Ohai output

All the information that Ohai collects on the instance, is saved in the failed-run-data.json file, even if there is no error. It is a great resource to get the server specific values.

Cloud info

cloud-specific information under “cloud“
cloud instance information under “ec2“
Computer info

CPU and memory configuration under “cpu“, “cs_info“, “memory“
drive sizes and network settings under “filesystem” and “network“
Operating system info

operating system information under “os_info“
list of enabled Windows features under “dism_features“
the list of installed applications under “packages“
Chef info

Chef client configuration values under “chef_client“
the Chef node information under “chef_type”: “node“
the Chef run list under “run_list“
list of Chef cookbooks and their versions under “cookbooks“
list of the executed recipes under “recipes“
the value of the passed in Chef attributes under “normal“
the value of the Chef cookbook attributes under “chef_client” and the cookbook name
all the information on the Chef resources under “json_class“
the stack trace of the last error under “exception“
 Debug mode

To get detailed information on the Chef run, start the Chef Client in debug mode. Execute the command in sudo mode, because many Chef steps require elevated rights.

chef-client -l debug -L debug.txt
The debug information is saved in the debug.txt file in the current folder.
